import * as services from 'core/services';

const injected = {};

export class Base {
    constructor() {
        this.__subscriptions = [];
        this.__streams = [];
    }

    destroy() {
        this.__subscriptions.forEach(sbs => sbs.unsubscribe());
        this.__streams.forEach(str => str.destroy());

        delete this.__subscriptions;
        delete this.__streams;
    }

    inject() {
        let serviceNames = Array.prototype.slice.call(arguments);

        serviceNames.forEach(serviceName => {
           this[serviceName] = injected[serviceName] || new services[serviceName]();
        });
    }

    createStream(key, value) {
        if ( this[key] ) {
            this[key].destroy();
        }

        let newStream;

        if ( typeof(value) === 'undefined' ) {
            newStream = new Stream();
        } else {
            newStream = new BehaviourStream(value);
        }

        newStream.onDestroy.push(() => {
            let ind = this.__streams.indexOf(newStream);
            if ( ind >= 0 ) {
                this.__streams.splice(ind, 1);
            }
        });
        this.__streams.push(newStream);
        this[key] = newStream;
        return newStream;
    }

    createStreams() {
        Array.prototype.slice.call(arguments).forEach(key => {
            if ( typeof(key) === 'string') {
                this._createStream(key);
            } else {
                this._createStream(key[0], key[1]);
            }
        });
    }

    subscribe(stream, worker) {
        let subscription;
        if (typeof(stream) === 'string' ) {
            if ( !this[stream] ) { return null; }
            subscription = this[stream].subscribe(worker);
        } else {
            subscription = stream.subscribe(worker);
        }
        this.__subscriptions.push(subscription);
        return subscription;
    }
}

window.Base = Base;
