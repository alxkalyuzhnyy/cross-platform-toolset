#Cross-platform Toolset

This is a toolset currently used by me in day-to-day work.

Logic and UI separated into different processes (with service worker), which allows for app initialization (auth happens in background and remains active) and makes it possible to build app for various platforms (PWA and rbowser plugin in this case)

Uses firebase as backend and publishing platform, requires authorization

####Features:
* Shared notes with tree-like hierarchy
* Calculator (WIP)
* Minimal browser (scaled down iframe, nice for news check)
* Calendar that fetches information from several accounts
* GPS map (WIP)
* Updates tracker - WIP, tool to check updates automatically (new articles on a web site for instance) and notify with push message

####Screens:
![Browser plugin view](README/toolset.jpeg)
![Mobile PWA view](README/toolset2.jpeg)
![Map as plugin](README/toolset3.jpeg)

####Getting started
```bash
npm install
npm start
```
